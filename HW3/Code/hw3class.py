from scipy.io import loadmat
import numpy as np
import tqdm
from pylab import pcolor, show, colorbar, xticks, yticks
import matplotlib.pyplot as plt
mnistMain = loadmat("hw3_mnist_dist/train.mat")['trainX']
# mnistMainLabels = mnistMain.T[-1].T
# mnistMain = mnistMain.T[:-1].T
#combine all label 1s
firstFs = []
for row in tqdm.tqdm(mnistMain):
    eas = list(row)
    lastIndex = eas.pop()
    if lastIndex == 0:
        normOfRow = np.linalg.norm(np.array(eas))
        eas = [i/normOfRow for i in eas]
        firstFs.append(eas)
print("Calculating Covariance Matrix")

covMatrix = np.cov(firstFs,rowvar=False)

# plotting the correlation matrix
plt.imshow(covMatrix, cmap='hot', interpolation='nearest')
plt.show()
