"""
Illustrate simple contour plotting, contours on an image with
a colorbar for the contours, and labelled contours.

See also contour_image.py.
"""
import matplotlib
import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import math

matplotlib.rcParams['xtick.direction'] = 'out'
matplotlib.rcParams['ytick.direction'] = 'out'

delta = 0.025
x = np.arange(-5, 5.0, delta)
y = np.arange(-5, 7, delta)
X, Y = np.meshgrid(x, y)
q1 = mlab.bivariate_normal(X, Y, 1.0, math.sqrt(2.0), 1.0, 1.0)
q2 = mlab.bivariate_normal(X, Y, math.sqrt(2), math.sqrt(3), -1.0, 2.0, 1)
q3a = mlab.bivariate_normal(X, Y, math.sqrt(2), 1.0, 0.0, 2.0, 1)
q3b = mlab.bivariate_normal(X, Y, math.sqrt(2), 1.0, 2.0, 0.0, 1)
q4a = mlab.bivariate_normal(X, Y, math.sqrt(2), 1.0, 0.0, 2.0, 1)
q4b = mlab.bivariate_normal(X, Y, math.sqrt(2), math.sqrt(3), 2.0, 0.0, 1)
q5a = mlab.bivariate_normal(X, Y, math.sqrt(2), 1.0, 1.0, 1.0, 0)
q5b = mlab.bivariate_normal(X, Y, math.sqrt(2), math.sqrt(2), -1.0, -1.0, 1)
Z2 = mlab.bivariate_normal(X, Y, 1.5, 0.5, 1, 1)
# difference of Gaussians
# Z = 10.0 * (Z2 - Z1)
# Z = q5a - q5b
Z = q5a - q5b


# Create a simple contour plot with labels using default colors.  The
# inline argument to clabel will control whether the labels are draw
# over the line segments of the contour, removing the lines beneath
# the label
plt.figure()
CS = plt.contour(X, Y, Z)
plt.clabel(CS, inline=1, fontsize=10)
plt.title('IsoContour')



plt.show()
