import numpy as np
import matplotlib.pyplot as plt
#Draw Samples and calculate mean
x1 = np.random.normal(3,3,100)
x2 = np.random.normal(4,2,100)
for i,n39 in enumerate(x1):
    x2[i] = .5 * x1[i] + x2[i]
x1mean = np.mean(x1,axis=0)
x2mean = np.mean(x2, axis=0)
print("Mean is " + str((x1mean,x2mean)))

varx = np.var(x1)
vary = np.var(x2)
covxy = np.cov(np.vstack((x1,x2)))
# print("Variance is " + str((varx, vary)) + ". Covariance is " +str(covxy))
print(covxy)
w, v = np.linalg.eig(covxy)
eig1 = w[0]
eig2 = w[1]
eigV1 = v[:,0]
eigV2 = v[:,1]
v1 = eig1 * eigV1
v2 = eig2 * eigV2
print(eig1)
print(eig2)
print(eigV1)
print(eigV2)
# plt.figure(0)
# plt.scatter(x1, x2)
# plt.axis([-15, 15, -15, 15])
# ax = plt.axes()
# ax.arrow(x1mean, x2mean, v1[0], v1[1], head_width=0.1, head_length=0.1, fc='k', ec='k')
# ax.arrow(x1mean, x2mean, v2[1], v2[1], head_width=0.1, head_length=0.1, fc='k', ec='k')
#
# plt.show()

U = np.matrix([[v1[0], v2[0]], [v1[1], v2[1]]])
points = np.vstack((x1,x2)).T
for i, point in enumerate(points):
    prevPoint = points[i]
    prevPoint[0] = prevPoint[0] - x1mean
    prevPoint[1] = prevPoint[1] - x2mean
    points[i] = U.T.dot(prevPoint)
# plt.figure(1)
plt.axis([-15, 15, -15, 15])
plt.scatter(points[:,0], points[:,1])
plt.show()
