from scipy.io import loadmat
import tqdm
import numpy as np
import pickle
import random
import math
import csv
import sys
import copy
#SETUP
filePresent = False
if not filePresent:
    mnistMain = loadmat("hw3_mnist_dist/train.mat")['trainX']
    mnistTest = loadmat("hw3_mnist_dist/test.mat")['testX']
    classesDict = {}
    print("DATA LOADED")
    for i in range(10):
        classesDict[i] = []
    # for row in tqdm.tqdm(mnistMain):
    for row in mnistMain:
        eas = list(row)
        lastIndex = eas.pop()
        normOfRow = np.linalg.norm(np.array(eas))
        eas = [i/normOfRow for i in eas]
        classesDict[lastIndex].append(eas)
    # pickle.dump( classesDict, open( "save.p", "wb" ) )
else:
    classesDict = pickle.load( open( "save.p", "rb" ) )
print("STUFF LOADED")
print("Classes Differentiated")
def pickAndPopRandom(someDict):
    classToPickFrom = random.randint(0,9)
    lengthOfClass = len(someDict[classToPickFrom])
    while (lengthOfClass == 0):
        classToPickFrom = random.randint(0,9)
        lengthOfClass = len(someDict[classToPickFrom])
    exactOption = random.randint(0, lengthOfClass - 1)
    classT = someDict[classToPickFrom].pop(exactOption)
    return (classToPickFrom, classT)

def trainAndClassify(nSamples, c):
    classesDictCopy = {}

    for i in range(10):
        classesDictCopy[i] = copy.copy(classesDict[i])
    print("Duplicated Class Dict")
    trainDict = {}
    for i in range(10):
        trainDict[i] = []
    validationTuples = []
    print("Forming Validation Set")
    # for val in tqdm.tqdm(range(10000)):
    for val in range(10000):
        validationTuples.append(pickAndPopRandom(classesDictCopy))
    print("Forming Samples Set")
    for samp in range(nSamples):
    # for samp in tqdm.tqdm(range(nSamples)):
        TupC = pickAndPopRandom(classesDictCopy)
        trainDict[TupC[0]].append(TupC[1])
    means = []
    for i in range(10):
        meanClass = np.mean(trainDict[i], axis=0)
        means.append(meanClass)
    sigmaMatrix = np.zeros((784, 784))

    for classI in range(10):
        classMatrix = np.zeros((784, 784))
        totalClasses = trainDict[classI]
        relevantMean = means[classI]
        # for classX in tqdm.tqdm(totalClasses):
        for classX in totalClasses:
            numpified = np.array(classX)
            toMult = numpified - relevantMean
            classMatrix += toMult.dot(toMult.T)
        sigmaMatrix += classMatrix
    print("Sigma Done")
    sigmaMatrix = float(1/nSamples) * sigmaMatrix
    try:
        sigmaInverse = np.linalg.inv(sigmaMatrix)
    except Exception as e:
        sigmaInverse = np.linalg.inv(sigmaMatrix + c * np.identity(784))
    else:
        pass
    #validate
    correct = 0
    print("validating")
    for VTup in validationTuples:
        prior = float(len(trainDict[classI]))/nSamples
        classV = VTup[0]
        x = VTup[1]
        classifs = []
        for cI in range(10):
            CM = means[cI]
            a = CM.T.dot(sigmaInverse).dot(np.array(x))
            b = -.5 * CM.T.dot(sigmaInverse).dot(CM) + math.log(prior)
            classifs.append((a + b))
        if np.argmax(classifs) == classV:
            correct += 1

    print("NUM CORRECT: " + str(float(correct)/10000))
    return float(correct)/10000
# for NUsamples in [100, 200, 500, 1,000, 2,000, 5,000, 10,000, 30,000, 50,000]:
# for c in [000001]:
#     trainAndClassify(50000, c)


def trainAndTest(c):

    means = []
    for i in range(10):
        meanClass = np.mean(classesDict[i], axis=0)
        means.append(meanClass)
    sigmaMatrix = np.zeros((784, 784))
    for classI in range(10):
        classMatrix = np.zeros((784, 784))
        totalClasses = classesDict[classI]
        relevantMean = means[classI]
        for classX in totalClasses:
            numpified = np.array(classX)
            toMult = numpified - relevantMean
            classMatrix += toMult.dot(toMult.T)
        sigmaMatrix += classMatrix
    print("Sigma Done")
    sigmaMatrix = float(1/60000) * sigmaMatrix
    try:
        sigmaInverse = np.linalg.inv(sigmaMatrix)
    except Exception as e:
        sigmaInverse = np.linalg.inv(sigmaMatrix + c * np.identity(784))
    else:
        pass
    #validate
    correct = 0
    print("validating")
    answers = []
    for testV in mnistTest:
        prior = float(len(classesDict[classI]))/60000
        classifs = []
        for cI in range(10):
            CM = means[cI]
            normOfRow = np.linalg.norm(testV)
            eas = np.array([i/normOfRow for i in list(testV)])
            a = CM.T.dot(sigmaInverse).dot(np.array(eas))
            b = -.5 * CM.T.dot(sigmaInverse).dot(CM) + math.log(prior)
            classifs.append((a + b))
        answers.append(np.argmax(classifs))
    f = open('hw3outmnist.csv', 'wt')
    try:
        writer = csv.writer(f)
        writer.writerow( ('Id', 'Category') )
        for i in range(10000):
            writer.writerow( (i, str(answers[i])) )
    finally:
        f.close()

trainAndTest(.000001)
#Testing C Values
# c = .05
# maxV = 0
# macC = .05
# while c <= 1:
#     print(c)
#     try:
#         fd = trainAndClassify(45000, c)
#         if fd > maxV:
#             maxV = fd
#             macC = c
#     except Exception as e:
#         pass
#     else:
#         pass
#     c += .05
# print(macC)
# print(maxV)
