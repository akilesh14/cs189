from scipy.io import loadmat
from scipy.stats import multivariate_normal
from sklearn.preprocessing import normalize
import tqdm
import numpy as np
import pickle
import random
import math
import csv
import sys
import copy

mnistMain = loadmat("hw3_mnist_dist/train.mat")['trainX']
def validate(tSize):
    print("Classes Indexed")
    validationSize = 10000
    trainingSize = tSize
    validationLabels = []
    validationSet = []
    alreadyUsed = set()
    print("Creating Validation")
    for vI in tqdm.tqdm(range(validationSize)):
        uniformR = random.randint(0,len(mnistMain) - 1)
        while uniformR in alreadyUsed:
            uniformR = random.randint(0,len(mnistMain) - 1)
        alreadyUsed.add(uniformR)
        vV = mnistMain[uniformR]
        vClass = vV[-1]
        vVActual = vV[:-1]
        vvNormed = normalize(vVActual.astype('float64').reshape(1,-1))
        validationSet.append(vvNormed[0])
        validationLabels.append(vClass)
    trainingSet = [[] for i in range(10)]
    print("Creating Training")
    for vI in tqdm.tqdm(range(trainingSize)):
        uniformR = random.randint(0,len(mnistMain) - 1)
        while uniformR in alreadyUsed:
            uniformR = random.randint(0,len(mnistMain) - 1)
        alreadyUsed.add(uniformR)
        vV = mnistMain[uniformR]
        vClass = vV[-1]
        vVActual = vV[:-1]
        vvNormed = normalize(vVActual.astype('float64').reshape(1, -1))
        trainingSet[vClass].append(vvNormed[0])

    sumCov = np.zeros((784,784))
    print("Calcing COVS")
    for classT in tqdm.tqdm(range(10)):
        covM = np.cov(trainingSet[classT], rowvar=False)
        sumCov += covM
    sumCov = .1 * sumCov
    means = []
    for i in range(10):
        meanClass = np.mean(trainingSet[i], axis=0)
        means.append(meanClass)
    print("Calcing Multis")
    multivariates =  []
    for mvm in tqdm.tqdm(range(10)):
        try:
            multivariates.append(multivariate_normal(mean=means[mvm], cov=sumCov))
        except Exception as e:
            sumCov = sumCov + .01 * np.identity(784)
            multivariates.append(multivariate_normal(mean=means[mvm], cov=sumCov))
        else:
            pass
    correct = 0
    print("Calculating Validation")
    for i, vectorV in tqdm.tqdm(enumerate(validationSet)):
        pds = [mul.logpdf(vectorV) for mul in multivariates]
        if np.argmax(pds) == validationLabels[i]:
            correct += 1
    print(float(correct)/10000)

def testAndPrint():
    print("Classes Indexed")

    alreadyUsed = set()
    print("Creating Validation")
    trainingSet = [[] for i in range(10)]
    print("Creating Training")
    for vI in tqdm.tqdm(range(60000)):
        uniformR = random.randint(0,len(mnistMain) - 1)
        while uniformR in alreadyUsed:
            uniformR = random.randint(0,len(mnistMain) - 1)
        alreadyUsed.add(uniformR)
        vV = mnistMain[uniformR]
        vClass = vV[-1]
        vVActual = vV[:-1]
        vvNormed = normalize(vVActual.astype('float64').reshape(1, -1))
        trainingSet[vClass].append(vvNormed[0])

    sumCov = np.zeros((784,784))
    print("Calcing COVS")
    for classT in tqdm.tqdm(range(10)):
        covM = np.cov(trainingSet[classT], rowvar=False)
        sumCov += covM
    sumCov = .1 * sumCov
    means = []
    for i in range(10):
        meanClass = np.mean(trainingSet[i], axis=0)
        means.append(meanClass)
    print("Calcing Multis")
    multivariates =  []
    for mvm in tqdm.tqdm(range(10)):
        try:
            multivariates.append(multivariate_normal(mean=means[mvm], cov=sumCov))
        except Exception as e:
            sumCov = sumCov + .01 * np.identity(784)
            multivariates.append(multivariate_normal(mean=means[mvm], cov=sumCov))
        else:
            pass
    correct = 0
    print("Calculating Test")
    mnistTest = loadmat("hw3_mnist_dist/test.mat")['testX']
    answers = []
    for i, vectorV in tqdm.tqdm(enumerate(mnistTest)):
        vvNormed = normalize(vectorV.astype('float64').reshape(1,-1))[0]
        pds = [mul.logpdf(vvNormed) for mul in multivariates]
        answers.append(np.argmax(pds))
        f = open('hw3outmnist.csv', 'wt')
    try:
        writer = csv.writer(f)
        writer.writerow( ('Id', 'Category') )
        for i in range(10000):
            writer.writerow( (i, str(answers[i])) )
    finally:
        f.close()

testAndPrint()
