from scipy.io import loadmat
import tqdm
import numpy as np
import pickle
import random
import math
import copy
#SETUP
filePresent = False
if not filePresent:
    mnistMain = loadmat("hw3_mnist_dist/train.mat")['trainX']
    classesDict = {}
    print("DATA LOADED")
    for i in range(10):
        classesDict[i] = []
    # for row in tqdm.tqdm(mnistMain):
    for row in mnistMain:
        eas = list(row)
        lastIndex = eas.pop()
        normOfRow = np.linalg.norm(np.array(eas))
        eas = [i/normOfRow for i in eas]
        classesDict[lastIndex].append(eas)
    # pickle.dump( classesDict, open( "save.p", "wb" ) )
else:
    classesDict = pickle.load( open( "save.p", "rb" ) )
print("STUFF LOADED")
print("Classes Differentiated")
def pickAndPopRandom(someDict):
    classToPickFrom = random.randint(0,9)
    lengthOfClass = len(someDict[classToPickFrom])
    while (lengthOfClass == 0):
        classToPickFrom = random.randint(0,9)
        lengthOfClass = len(someDict[classToPickFrom])
    exactOption = random.randint(0, lengthOfClass - 1)
    classT = someDict[classToPickFrom].pop(exactOption)
    return (classToPickFrom, classT)

def trainAndClassify(nSamples):
    classesDictCopy = {}
    for i in range(10):
        classesDictCopy[i] = copy.copy(classesDict[i])
    print("Duplicated Class Dict")
    trainDict = {}
    for i in range(10):
        trainDict[i] = []
    validationTuples = []
    print("Forming Validation Set")
    # for val in tqdm.tqdm(range(10000)):
    for val in range(10000):
        validationTuples.append(pickAndPopRandom(classesDictCopy))
    print("Forming Samples Set")
    for samp in range(nSamples):
    # for samp in tqdm.tqdm(range(nSamples)):
        TupC = pickAndPopRandom(classesDictCopy)
        trainDict[TupC[0]].append(TupC[1])
    means = []
    for i in range(10):
        meanClass = np.mean(trainDict[i], axis=0)
        means.append(meanClass)
    sigmaMatrices = []

    for classI in range(10):
        classMatrix = np.zeros((784, 784))
        totalClasses = trainDict[classI]
        relevantMean = means[classI]
        # for classX in tqdm.tqdm(totalClasses):
        for classX in totalClasses:
            numpified = np.array(classX)
            toMult = numpified - relevantMean
            classMatrix += toMult.dot(toMult.T)
        sigmaMatrices.append(1/float(len(trainDict[classI])) * classMatrix)
    print("Sigma Done")
    determinants = []
    for indeixe in range(len(sigmaMatrices)):
    # try:
        determinant = np.linalg.det(sigmaMatrices[indeixe])
        if determinant == 0:
            sigmaMatrices[indeixe] = sigmaMatrices[indeixe] + np.identity(784)
            determinant = np.linalg.det(sigmaMatrices[indeixe])
        if determinant == 0:
            w, v = np.linalg.eig(sigmaMatrices[indeixe])
            determinant = sum([math.log(eigenvlaue) for eigenvlaue in w])
        sigmaMatrices[indeixe] = np.linalg.inv(sigmaMatrices[indeixe])
        determinants.append(determinant)
    # except Exception as e:
        # determinants[indeixe] = np.linalg.det(sigmaMatrices[indeixe])
        # sigmaMatrices[indeixe] = np.linalg.inv(sigmaMatrices[indeixe] + .6 * np.identity(784))
    #     print(e)
    # else:
    #     pass
    #validate
    correct = 0
    print("validating")
    for VTup in validationTuples:
        prior = float(len(trainDict[classI]))/nSamples
        classV = VTup[0]
        x = VTup[1]
        classifs = []
        for cI in range(10):
            sigmaInverse = sigmaMatrices[cI]
            CM = means[cI]
            meanminus = x - CM
            a = .5 * meanminus.T.dot(sigmaInverse).dot(meanminus)
            b = .5 * math.log(determinants[cI])
            c = math.log(prior)
            classifs.append(-a - b + c)
        if np.argmax(classifs) == classV:
            correct += 1
    print("NUM CORRECT: " + str(float(correct)/10000))
for NUsamples in [30000]:
# for NUsamples in [100, 200, 500, 1,000, 2,000, 5,000, 10,000, 30,000, 50,000]:
    trainAndClassify(NUsamples)
