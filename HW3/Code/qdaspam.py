from scipy.io import loadmat
from scipy.stats import multivariate_normal
from sklearn.preprocessing import normalize
import tqdm
import numpy as np
import pickle
import random
import math
import csv
import sys
import copy

spamDict = loadmat("spam_data.mat")
spamTrain = spamDict['training_data']
spamLabels = spamDict['training_labels']
spamFull = np.vstack((spamTrain.T, spamLabels[0]))
spamFull = spamFull.T

def validate(tSize):
    print("Classes Indexed")
    validationSize = 10000
    trainingSize = tSize
    validationLabels = []
    validationSet = []
    alreadyUsed = set()
    print("Creating Validation")
    for vI in tqdm.tqdm(range(validationSize)):
        uniformR = random.randint(0,len(spamFull) - 1)
        while uniformR in alreadyUsed:
            uniformR = random.randint(0,len(spamFull) - 1)
        alreadyUsed.add(uniformR)
        vV = spamFull[uniformR]
        vClass = int(vV[-1])
        vVActual = vV[:-1]
        vvNormed = normalize(vVActual.astype('float64').reshape(1,-1))
        validationSet.append(vvNormed[0])
        validationLabels.append(vClass)
    trainingSet = [[],[]]
    print("Creating Training")
    for vI in tqdm.tqdm(range(trainingSize)):
        uniformR = random.randint(0,len(spamFull) - 1)
        while uniformR in alreadyUsed:
            uniformR = random.randint(0,len(spamFull) - 1)
        alreadyUsed.add(uniformR)
        vV = spamFull[uniformR]
        vClass = int(vV[-1])
        vVActual = vV[:-1]
        vvNormed = normalize(vVActual.astype('float64').reshape(1, -1))
        trainingSet[vClass].append(vvNormed[0])

    print("Calcing COVS")
    covs = []
    for classT in tqdm.tqdm(range(2)):
        covM = np.cov(trainingSet[classT], rowvar=False)
        covs.append(1/10.0 * covM)
        # covs.append(1/float(len(trainingSet[classT])) * covM)

    #
    means = []
    for i in range(2):
        meanClass = np.mean(trainingSet[i], axis=0)
        means.append(meanClass)
    print("Calcing Multis")
    multivariates =  []
    for mvm in tqdm.tqdm(range(2)):
        try:
            multivariates.append(multivariate_normal(mean=means[mvm], cov=covs[mvm]))
        except Exception as e:
            covs[mvm] = covs[mvm] + .00001 * np.identity(32)
            multivariates.append(multivariate_normal(mean=means[mvm], cov=covs[mvm]))
        else:
            pass
    correct = 0
    print("Calculating Validation")
    for i, vectorV in tqdm.tqdm(enumerate(validationSet)):
        pds = [mul.logpdf(vectorV) for mul in multivariates]
        if np.argmax(pds) == validationLabels[i]:
            correct += 1
    print(float(correct)/validationSize)



def testAndPrint():
    print("Classes Indexed")

    alreadyUsed = set()
    trainingSet = [[],[]]
    print("Creating Training")
    for vI in tqdm.tqdm(range(23702)):
        uniformR = random.randint(0,len(spamFull) - 1)
        while uniformR in alreadyUsed:
            uniformR = random.randint(0,len(spamFull) - 1)
        alreadyUsed.add(uniformR)
        vV = spamFull[uniformR]
        vClass = int(vV[-1])
        vVActual = vV[:-1]
        vvNormed = normalize(vVActual.astype('float64').reshape(1, -1))
        trainingSet[vClass].append(vvNormed[0])
    print("Calcing COVS")
    covs = []
    for classT in tqdm.tqdm(range(2)):
        covM = np.cov(trainingSet[classT], rowvar=False)
        covs.append(1/10.0 * covM)
    #float(len(trainingSet[classT]))
    means = []
    for i in range(2):
        meanClass = np.mean(trainingSet[i], axis=0)
        means.append(meanClass)
    print("Calcing Multis")
    multivariates =  []
    for mvm in tqdm.tqdm(range(2)):
        try:
            multivariates.append(multivariate_normal(mean=means[mvm], cov=covs[mvm]))
        except Exception as e:
            covs[mvm] = covs[mvm] + .00001 * np.identity(32)
            multivariates.append(multivariate_normal(mean=means[mvm], cov=covs[mvm]))
        else:
            pass
    correct = 0
    print("Calculating Test")
    mnistTest = loadmat("spam_data.mat")['test_data']
    answers = []
    for i, vectorV in tqdm.tqdm(enumerate(mnistTest)):
        vvNormed = normalize(vectorV.astype('float64').reshape(1,-1))[0]
        pds = [mul.logpdf(vvNormed) for mul in multivariates]
        answers.append(np.argmax(pds))
    f = open('hw5spamdtree.csv', 'wt')
    try:
        writer = csv.writer(f)
        writer.writerow( ('Id', 'Category') )
        for i in range(10000):
            writer.writerow( (i, str(answers[i])) )
    finally:
        f.close()
for v in [100,200,500,1000,2000,5000,10000,13702]:
    validate(v)
