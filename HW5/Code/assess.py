from scipy.io import loadmat
from randomForests import *

import time
from random import randint
import tqdm
import csv



def assess(trainingSize, validationSize, training, labels, testD, decisionMaker, test=False, filename='test.csv'):

    chosen = set()
    trainingData = []
    trainingLabels = []
    validationData = []
    validationLabels = []
    for i in tqdm.tqdm(range(trainingSize)):
        choice = randint(0, len(training) - 1)
        while choice in chosen:
            choice = randint(0,len(training) - 1)
        chosen.add(choice)
        trainingData.append(training[choice])
        trainingLabels.append(labels[choice])

    for j in tqdm.tqdm(range(validationSize)):
        choice = randint(0, len(training) - 1)
        while choice in chosen:
            choice = randint(0,len(training) - 1)
        chosen.add(choice)
        validationData.append(training[choice])
        validationLabels.append(labels[choice])

    start_time = time.time()
    # try:
    decisionMaker.train(np.array(trainingData), np.array(trainingLabels))
    # except Exception as e:
    #     print(e)
    elapsed_time = time.time() - start_time
    print("TRAINED")
    print(elapsed_time)

    print("Training Accuracy:")
    correctNum = 0
    for i,point in tqdm.tqdm(enumerate(trainingData)):
        if trainingLabels[i] == decisionMaker.predict(point):
            correctNum += 1
    print(float(correctNum)/len(trainingData))

    print("Validation Accuracy:")
    correctNum = 0
    for i,point in tqdm.tqdm(enumerate(validationData)):
        if validationLabels[i] == decisionMaker.predict(point):
            correctNum += 1
    print(float(correctNum)/(len(validationData) + 1))

    if test:
        answers = []
        for i, point in tqdm.tqdm(enumerate(testD)):
            answers.append(decisionMaker.predict(point))
        f = open(filename, 'wt')
        try:
            writer = csv.writer(f)
            writer.writerow( ('Id', 'Category') )
            for i in range(len(testD)):
                writer.writerow( (i, str(answers[i])) )
        finally:
            f.close()
def assessDLs(trainingSize, validationSize, training, labels):

    chosen = set()
    trainingData = []
    trainingLabels = []
    validationData = []
    validationLabels = []
    for i in tqdm.tqdm(range(trainingSize)):
        choice = randint(0, len(training) - 1)
        while choice in chosen:
            choice = randint(0,len(training) - 1)
        chosen.add(choice)
        trainingData.append(training[choice])
        trainingLabels.append(labels[choice])

    for j in tqdm.tqdm(range(validationSize)):
        choice = randint(0, len(training) - 1)
        while choice in chosen:
            choice = randint(0,len(training) - 1)
        chosen.add(choice)
        validationData.append(training[choice])
        validationLabels.append(labels[choice])

    # try:
    validations = []
    for i in tqdm.tqdm(range(40)):
        # start_time = time.time()
        decisionMaker = DecisionTree(depthLimit=i+1)
        decisionMaker.train(np.array(trainingData), np.array(trainingLabels))
    # except Exception as e:
    #     print(e)
        # print("TRAINED")
        # print(elapsed_time)
        # print("Validation Accuracy:")
        correctNum = 0
        for i,point in tqdm.tqdm(enumerate(validationData)):
            if validationLabels[i] == decisionMaker.predict(point):
                correctNum += 1
        validations.append(float(correctNum)/(len(validationData) + 1))
    return validations
