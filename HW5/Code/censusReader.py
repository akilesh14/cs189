import csv
import pandas
import tqdm
import statistics
from sklearn.feature_extraction import DictVectorizer
from randomForests import *
import assess
import math
a = pandas.read_csv('hw5_census_dist/train_data.csv').to_dict(orient='records')
b = pandas.read_csv('hw5_census_dist/test_data.csv').to_dict(orient='records')
labels = []
for datapoint in a:
    labels.append(datapoint.pop('label'))
a = a + b
featureDict = {}
for key in a[0].keys():
    featureDict[key] = []
for datapoint in tqdm.tqdm(a):
    for key in featureDict:
        if datapoint[key] != '?':
            featureDict[key].append(datapoint[key])
for key in tqdm.tqdm(featureDict.keys()):
    correspondingArray = featureDict[key]
    if type(correspondingArray[0]) == int:
        featureDict[key] =  statistics.mean(correspondingArray)
    else:
        featureDict[key] = statistics.mode(correspondingArray)
markCount = 0
for datapoint in tqdm.tqdm(a):
    for key in featureDict:
        if datapoint[key] == '?':
            markCount += 1
            datapoint[key] = featureDict[key]
vectored = DictVectorizer(sparse=False)
X = vectored.fit_transform(a)
training = X[:(len(a) - len(b))]
test = X[-len(b):]
#Labels in labels
#Datapoints in x

# dTree = DecisionTree(depthLimit=10)
# rF = randomForest(int(math.sqrt(training.shape[1])), 50, int(.25 * training.shape[0]), depthLimit=3)
# assess.assess(22724, 10000, training, labels, test, rF, True, 'censusDTree.csv')
# mostcommonsplits = []
# for tree in rF.trees:
#     mostcommonsplits.append((tree.root.splitFeature, tree.root.splitValue))
training.shape[0]
tSize = int(.8 * training.shape[0])
vSize = training.shape[0] - tSize
vData = assess.assessDLs(tSize, vSize, training, labels)
