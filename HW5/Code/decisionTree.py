import math
import statistics
from random import randint
class Node:
    def __init__(self, SF, SV, dataI = None):
        self.left = None
        self.right = None
        self.splitFeature = SF
        self.splitValue = SV
        self.dataIndices = dataI

class DecisionTree:
    def __init__(self, m=0, depthLimit=0):
        self.root = None
        self.data = None
        self.labels = None
        self.m = m
        self.depthLimit = depthLimit
        return
    def protectedLog(self, x):
        if x == 0:
            return 0
        else:
            return math.log(x, 2)
    def entropy(self, someHist, total):
        e = 0
        for certainClass in someHist.keys():
            classAmount = someHist[certainClass]
            pC = classAmount/float(total)
            e -= pC * self.protectedLog(pC)
        return e
    def impurity(self, left_label_hist,right_label_hist, tL, tR):
        #Histograms are Dictionaries of class value: amount in class, "total": total amount
        leftEntropy = self.entropy(left_label_hist, tL)
        rightEntropy = self.entropy(right_label_hist, tR)
        totalNumerator = tL * leftEntropy + tR * rightEntropy
        totalDenominator = tL + tR
        return totalNumerator/totalDenominator
    def segmenter(self,selectedIndices):
        minE = float('inf')
        minF = 0
        minV = 0
        featureRange = list(range(self.data.shape[1]))
        if self.m != 0:
            for removal in range(self.m):
                featureRange.remove(featureRange[randint(0,len(featureRange) - 1)])
        for feature in featureRange:
            features = []
            labels = []
            for ind in selectedIndices:
                features.append(self.data[ind][feature])
                labels.append(self.labels[ind])
            maxL = 2
            for f in features:
                if len(str(f)) > maxL:
                    maxL = len(str(f))
            #Combine and sort
            zipped = list(zip(features, labels))
            radictsort(maxL, zipped)

            #Initialize Histograms
            histLeft = {0:0, 1:0}
            leftTotal = 0
            histRight = {0:0, 1:0}
            rightTotal = 0

            for i in zipped:
                histRight[i[1]] += 1
                rightTotal += 1
            # histLeft[zipped[0][1]] += 1
            # leftTotal += 1
            previous = zipped[0]
            for split in zipped:
                if split[0] != previous[0]:
                    val = self.impurity(histLeft, histRight, leftTotal, rightTotal)
                    if val < minE:
                        minE = val
                        minV = split[0]
                        minF = feature
                    previous = split
                histRight[split[1]] -= 1
                histLeft[split[1]] += 1
                leftTotal += 1
                rightTotal -= 1
        if minE == float('inf') or minE < .10:
            minF = None
        return minF, minV
    def isLeaf(self, node):
        current = self.labels[node.dataIndices[0]]
        for i in node.dataIndices:
            if self.labels[i] != current:
                return False
        return True
    def generateChildren(self, node, depth):
        depthM = self.depthLimit > 0 and depth > self.depthLimit
        if not self.isLeaf(node) and not depthM:
            depth += 1
            sFeature = node.splitFeature
            sValue = node.splitValue
            leftDataI = []
            rightDataI = []
            for i in node.dataIndices:
                if self.data[i][sFeature] < sValue:
                    leftDataI.append(i)
                else:
                    rightDataI.append(i)
            # print(abs(len(leftDataI) - len(rightDataI)))
            if len(leftDataI) != 0:
                sLFeature, sLValue = self.segmenter(leftDataI)
                node.left = Node(sLFeature, sLValue, dataI=leftDataI)
                if len(leftDataI) != 1 and sLFeature != None:
                    self.generateChildren(node.left, depth)
            if len(rightDataI) != 0:
                sRFeature, sRValue = self.segmenter(rightDataI)
                node.right = Node(sRFeature, sRValue, dataI=rightDataI)
                if len(rightDataI) != 1 and sRFeature != None:
                    self.generateChildren(node.right, depth)

    def train(self ,data,labels):
        self.data = data
        self.labels = labels
        allData = [i for i in range(len(data))]
        sFeature, sValue = self.segmenter(allData)
        self.root = Node(sFeature, sValue, dataI=allData)
        self.generateChildren(self.root, 1)
        return
    def predict(self,data):
        current = self.root
        while current != None:
            if current.splitFeature == None:
                break
            if data[current.splitFeature] < current.splitValue:
                if current.left == None:
                    break
                current = current.left
            else:
                if current.right == None:
                    break
                current = current.right
        labelsFD = [self.labels[i]for i in current.dataIndices]
        try:
            common = statistics.mode(labelsFD)
        except Exception as e:
            common = labelsFD[0]
        return common
    def predictAndShow(self,data):
        current = self.root
        while current != None:
            if current.splitFeature == None:
                break
            if data[current.splitFeature] < current.splitValue:
                if current.left == None:
                    break
                print(str(current.splitFeature) + " < " + str(current.splitValue))
                current = current.left
            else:
                if current.right == None:
                    break
                print(str(current.splitFeature) + " >= " + str(current.splitValue))
                current = current.right
        labelsFD = [self.labels[i]for i in current.dataIndices]
        try:
            common = statistics.mode(labelsFD)
        except Exception as e:
            common = labelsFD[0]
        return common

class Histogram():
    def __init__(self):
        return


def radixsort(aList):
    RADIX = 10
    maxLength = False
    tmp, placement = -1, 1

    while not maxLength:
        maxLength = True
        # declare and initialize buckets
        buckets = [list() for _ in range(RADIX)]

        # split aList between lists
        for i in aList:
            tmp = i / placement
            buckets[tmp % RADIX].append(i)
            if maxLength and tmp > 0:
                maxLength = False

        # empty lists into aList array
        a = 0
        for b in range(RADIX):
            buck = buckets[b]
            for i in buck:
                aList[a] = i
                a += 1

        # move to next digit
        placement *= RADIX
def radictsort(r, aDict):
    RADIX = r
    maxLength = False
    tmp, placement = -1, 1

    while not maxLength:
        maxLength = True
        # declare and initialize buckets
        buckets = [list() for _ in range(RADIX)]

        # split aList between lists
        for i in aDict:
            tmp = int(i[0] / placement)
            buckets[tmp % RADIX].append(i)
            if maxLength and tmp > 0:
                maxLength = False

        # empty lists into aList array
        a = 0
        for b in range(RADIX):
            buck = buckets[b]
            for i in buck:
                aDict[a] = i
                a += 1

        # move to next digit
        placement *= RADIX

def incrementDict(someD, key):
    if key in someD:
        someD[key] += 1
    else:
        someD[key] = 1
