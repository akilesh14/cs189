from random import randint
import numpy as np
import tqdm
from decisionTree import *
import math
import statistics
class randomForest:
    def __init__(self, featureSubset, numberOfTrees, subsampleSize, depthLimit=0):
        self.m = featureSubset
        self.n = numberOfTrees
        self.s = subsampleSize
        self.depthLimit = depthLimit
        self.trees = []
    def train(self, data, labels):
        print("Making trees")
        for _ in tqdm.tqdm(range(self.n)):
            chosen = set()
            subsampledData = []
            subsampledLabels = []
            for j in range(self.s):
                choice = randint(0, len(data) - 1)
                while choice in chosen:
                    choice = randint(0, len(data) - 1)
                chosen.add(choice)
                subsampledData.append(data[choice])
                subsampledLabels.append(labels[choice])
            if self.depthLimit > 0:
                rTree = DecisionTree(m=self.m, depthLimit=self.depthLimit)
            else:
                rTree = DecisionTree(m=self.m)
            rTree.train(np.array(subsampledData), subsampledLabels)
            self.trees.append(rTree)
    def predict(self, point):
        predictions = []
        for tree in self.trees:
            predictions.append(tree.predict(point))
        try:
            return statistics.mode(predictions)
        except Exception as e:
            return predictions[0]
