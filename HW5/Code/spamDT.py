from scipy.io import loadmat
from decisionTree import *
import assess
spamDict = loadmat("dist2/spam_data.mat")
totaltrainingData = spamDict['training_data']
totaltrainingLabels = spamDict['training_labels'][0]
testData = spamDict['test_data']
normalDecisionTree = DecisionTree()
assess.assess(13702, 10000, totaltrainingData, totaltrainingLabels,testData, normalDecisionTree, False)

#Specific Point
