from scipy.io import loadmat
from randomForests import *
import assess
spamDict = loadmat("dist2/spam_data.mat")
totaltrainingData = spamDict['training_data']
totaltrainingLabels = spamDict['training_labels'][0]
testData = spamDict['test_data']
randomForest = randomForest(6,100,3425)
assess.assess(13702, 10000, totaltrainingData, totaltrainingLabels,testData, randomForest, False)
mostcommonsplits = []
for tree in randomForest.trees:
    mostcommonsplits.append((tree.root.splitFeature, tree.root.splitValue))
