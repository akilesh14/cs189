import csv
import pandas
import tqdm
import statistics
from sklearn.feature_extraction import DictVectorizer
from randomForests import *
import assess
import math
a = pandas.read_csv('hw5_titanic_dist/titanic_training.csv').to_dict(orient='records')
b = pandas.read_csv('hw5_titanic_dist/titanic_testing_data.csv').to_dict(orient='records')
labels = []
for datapoint in a:
    labels.append(datapoint.pop('survived'))
    datapoint.pop('ticket')
    datapoint.pop('cabin')
a = a + b
featureDict = {}
for key in a[0].keys():
    featureDict[key] = []
for datapoint in tqdm.tqdm(a):
    for key in featureDict:
        if datapoint[key] != '' and not (type(datapoint[key]) == float and math.isnan(datapoint[key])):
            featureDict[key].append(datapoint[key])
for key in tqdm.tqdm(featureDict.keys()):
    correspondingArray = featureDict[key]
    if type(correspondingArray[0]) == int:
        featureDict[key] =  statistics.mean(correspondingArray)
    else:
        featureDict[key] = statistics.mode(correspondingArray)
markCount = 0
for datapoint in tqdm.tqdm(a):
    for key in featureDict:
        if datapoint[key] == '' or (type(datapoint[key]) == float and math.isnan(datapoint[key])):
            markCount += 1
            datapoint[key] = featureDict[key]
vectored = DictVectorizer(sparse=False)
X = vectored.fit_transform(a)
training = X[:(len(a) - len(b))]
test = X[-len(b):]
labels[705] = 1.0
#Labels in labels
#Datapoints in x

dTree = DecisionTree(depthLimit=2)
assess.assess(1000, 0, training, labels, test, dTree, False, 'titanicDTree.csv')

# randomForest = randomForest(int(math.sqrt(training.shape[1])),10,int(800/4))
# assess.assess(800, 200, training, labels, test, randomForest, True, 'titanicDRTree.csv')
