
# coding: utf-8

# In[2]:

import numpy as np
import math


# In[3]:

X = np.array([[0,3],[1,3], [0,1],[1,1]])
y = np.array([1,1,0,0])
XBiased = np.vstack((X.T, [1,1,1,1])).T
wInit = np.array([-2,1,0])
n = X.shape[0]
print(XBiased)
l = .07


# In[4]:

def sigmoid(gamma):
    denom = 1 + math.exp(-gamma)
    return 1/denom


# In[5]:

def calcS(X, w):
    s = []
    for i in range(n):
        s.append(sigmoid(X[i].dot(w)))
    return s


# In[6]:

s0 = calcS(XBiased, wInit)
print(s0)


# In[8]:

sigmaMatrix = np.zeros((4,4))
for i in range(4):
    sigmaMatrix[i,i] = s0[i] * (1 - s0[i])
leftSide = 2 * l + XBiased.T.dot(sigmaMatrix).dot(XBiased)
rightside = -2 * l * wInit + XBiased.T.dot(y - s0)


# In[9]:

print(leftSide.shape)


# In[10]:

rightside.shape


# In[11]:

e = np.linalg.solve(leftSide, rightside)
wNew = wInit + e
print(wNew)


# In[12]:

s1 = calcS(XBiased, wNew)
print(s1)


# In[13]:

sigmaMatrix = np.zeros((4,4))
for i in range(4):
    sigmaMatrix[i,i] = s1[i] * (1 - s1[i])
leftSide = 2 * l * n + XBiased.T.dot(sigmaMatrix).dot(XBiased)
rightside = -2 * l * (wNew[0] + wNew[1] + wNew[2]) + XBiased.T.dot(y - s1)
e = np.linalg.solve(leftSide, rightside)
wNew = wNew + e
print(wNew)


# In[ ]:




# In[14]:

XBiased.dot(wNew)


# In[ ]:



