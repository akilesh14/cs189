from scipy.io import loadmat
import numpy as np
from sklearn.svm import SVC
from sklearn.svm import LinearSVC


#MNIST
def mnist():
    mnistMain = loadmat("hw01_data/mnist/train.mat")['trainX']
    np.random.shuffle(mnistMain)
    mnistValidation = mnistMain[:10000]
    mnistTraining = mnistMain[10000:]
    mvalidationLabels = mnistValidation.T[-1].T
    mvalidation = mnistValidation.T[:-1].T
    mnistLabels = mnistTraining.T[-1].T
    mnistTraining = mnistTraining.T[:-1].T
    print("MNIST Datasets Created")
    for trainingSize in [100, 200, 500, 1000, 2000, 5000,10000]:
        classifier = SVC(kernel='linear')
        # print("training Started for " + str(trainingSize))
        classifier.fit(mnistTraining[:trainingSize], mnistLabels[:trainingSize])
        # print("training Ended")
        print(1 - classifier.score(mvalidation, mvalidationLabels))

def checkAccuracy():
    mnistMain = loadmat("hw01_data/mnist/train.mat")['trainX']
    mnistTest = loadmat("hw01_data/mnist/test.mat")['testX']

    np.random.shuffle(mnistMain)
    mnistValidation = mnistMain[:10000]
    mnistTraining = mnistMain[10000:]
    mvalidationLabels = mnistValidation.T[-1].T
    mvalidation = mnistValidation.T[:-1].T
    mnistLabels = mnistTraining.T[-1].T
    mnistTraining = mnistTraining.T[:-1].T
    print("MNIST Datasets Created")
    classifier = SVC(kernel='linear')
    print("training Started for " + str(10000))
    classifier.fit(mnistTraining[:10000], mnistLabels[:10000])
    print("training Ended")
    answers = classifier.predict(mnistTest)
    import csv
    import sys

    f = open('mnistout.csv', 'wt')
    try:
        writer = csv.writer(f)
        writer.writerow( ('Id', 'Category') )
        for i in range(10000):
            writer.writerow( (i, str(answers[i])) )
    finally:
        f.close()
    return answers

def mnistWithCValueAltering():
    mnistMain = loadmat("hw01_data/mnist/train.mat")['trainX']
    np.random.shuffle(mnistMain)
    mnistValidation = mnistMain[:10000]
    mnistTraining = mnistMain[10000:]
    mvalidationLabels = mnistValidation.T[-1].T
    mvalidation = mnistValidation.T[:-1].T
    mnistLabels = mnistTraining.T[-1].T
    mnistTraining = mnistTraining.T[:-1].T
    print("MNIST Datasets Created")
    trainingSize = 1000
    for c in [1,1e-6,2e-6,3e-6,7e-6,10e-6]:
        classifier = SVC(kernel='linear', C=c)
        print("training Started for c value: " + str(c))
        classifier.fit(mnistTraining[:trainingSize], mnistLabels[:trainingSize])
        print("training Ended")
        print(classifier.score(mvalidation, mvalidationLabels))
def spam():
    spamDict = loadmat("hw01_data/spam/spam_data.mat")
    spamTrain = spamDict['training_data']
    spamLabels = spamDict['training_labels']
    spamFull = spamTrain.T + spamLabels[0]
    spamFull = spamFull.T
    np.random.shuffle(spamFull)
    numVal = int(.2*spamFull.shape[0])
    spamValidation = spamFull[:numVal]
    spamTraining = spamFull[numVal:]
    trainingLength = spamTraining.shape[0]
    spamvalidationLabels = spamValidation.T[-1].T
    spamvalidation = spamValidation.T[:-1].T
    spamLabels = spamTraining.T[-1].T
    spamTraining = spamTraining.T[:-1].T
    print("Spam Datasets Created")
    for trainingSize in [100, 200, 500, 1000, 2000, trainingLength]:
        classifier = SVC(kernel='linear')
        # print("training Started for " + str(trainingSize))
        classifier.fit(spamTraining[:trainingSize], spamLabels[:trainingSize])
        # print("training Ended")
        print(1 - classifier.score(spamTraining[:trainingSize], spamLabels[:trainingSize]))
def spamAc():
    spamDict = loadmat("hw01_data/spam/spam_data.mat")
    spamTrain = spamDict['training_data']
    spamLabels = spamDict['training_labels']
    testData = spamDict['test_data']
    spamFull = np.vstack((spamTrain.T,spamLabels[0]))
    spamFull = spamFull.T
    np.random.shuffle(spamFull)
    numVal = 0
    spamValidation = spamFull[:numVal]
    spamTraining = spamFull[numVal:]
    trainingLength = spamTraining.shape[0]
    spamvalidationLabels = spamValidation.T[-1].T
    spamvalidation = spamValidation.T[:-1].T
    spamLabels = spamTraining.T[-1].T
    spamTraining = spamTraining.T[:-1].T
    print("Spam Datasets Created")
    classifier = SVC(kernel='linear', C=1)
    print("training Started for " + str(trainingLength))
    classifier.fit(spamTraining[:  trainingLength], spamLabels[:trainingLength])
    print("training Ended")
    # print(classifier.score(spamvalidation, spamvalidationLabels))
    answers = classifier.predict(testData)
    import csv
    import sys

    f = open('spamout.csv', 'wt')
    try:
        writer = csv.writer(f)
        writer.writerow( ('Id', 'Category') )
        for i in range(len(answers)):
            writer.writerow( (i, str(answers[i])) )
    finally:
        f.close()
    return answers

def spamKCross():
    spamDict = loadmat("hw01_data/spam/spam_data.mat")
    spamTrain = spamDict['training_data']
    spamLabels = spamDict['training_labels']
    spamFull = spamTrain.T + spamLabels[0]
    spamFull = spamFull.T
    np.random.shuffle(spamFull)
    k = 5
    klength = int(spamFull.shape[0]/k)
    # spamValidation = spamFull[:numVal]
    kSets = [spamFull[x*klength:((x+1)*klength)] for x in range(5)]
    print("Spam Datasets Created")
    cVal = 1
    while cVal < 100:
        sumScores = 0.0
        print(cVal)
        for kIndex in range(5):
            spamValidation = kSets[kIndex]
            spamTraining = [itTuple[1] for itTuple in enumerate(kSets) if (itTuple[0] != kIndex)]
            spamTraining = np.concatenate((spamTraining[0], spamTraining[1], spamTraining[2], spamTraining[3]))
            spamvalidationLabels = spamValidation.T[-1].T
            spamvalidation = spamValidation.T[:-1].T
            spamLabels = spamTraining.T[-1].T
            spamTraining = spamTraining.T[:-1].T
            classifier = LinearSVC(C=cVal)
            # print("training Started for k: " + str(kIndex))
            classifier.fit(spamTraining, spamLabels)
            # print("fitting")
            sumScores += classifier.score(spamvalidation, spamvalidationLabels)
        print(sumScores/k)
        cVal +=1
    # print('0.865183752418')


def cifar():
    cfarMain = loadmat("hw01_data/cifar/train.mat")['trainX']
    np.random.shuffle(cfarMain)
    cfarValidation = cfarMain[:5000]
    cfarTraining = cfarMain[5000:]
    cvalidationLabels = cfarValidation.T[-1].T
    cvalidation = cfarValidation.T[:-1].T
    cLabels = cfarTraining.T[-1].T
    cTraining = cfarTraining.T[:-1].T
    print("Cifar Datasets Created")
    for trainingSize in [100, 200, 500, 1000, 2000, 5000]:
        classifier = SVC(kernel='linear')
        # print("training Started for " + str(trainingSize))
        classifier.fit(cTraining[:trainingSize], cLabels[:trainingSize])
        # print("training Ended")
        print(1 - classifier.score(cTraining[:trainingSize], cLabels[:trainingSize]))
mnist()
spam()
cifar()
